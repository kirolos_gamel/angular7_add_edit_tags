import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdcashComponent } from './adcash/adcash.component';

const routes: Routes = [
  {
    path: '',
    component: AdcashComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
