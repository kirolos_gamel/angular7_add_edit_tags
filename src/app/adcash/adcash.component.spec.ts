import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdcashComponent } from './adcash.component';

describe('AdcashComponent', () => {
  let component: AdcashComponent;
  let fixture: ComponentFixture<AdcashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdcashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdcashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
