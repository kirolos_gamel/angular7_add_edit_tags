import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-adcash',
  templateUrl: './adcash.component.html',
  styleUrls: ['./adcash.component.scss']
})
export class AdcashComponent implements OnInit {

  adCashForm: FormGroup;
  updateAdCashForm: FormGroup;
  tagsArray = [];
  modalRef: BsModalRef;

  constructor(private fb: FormBuilder,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.adCashForm = this.fb.group({
      tags: ['',Validators.required],
    });
    this.updateAdCashForm = this.fb.group({
      tags: ['',Validators.required],
    });
    if(localStorage.getItem('tags')){
      this.tagsArray = JSON.parse(localStorage.getItem('tags'));
    }
    
  }

  addTags(){
    this.tagsArray = [];
    let tags = this.adCashForm.value.tags;
    let replaceString = tags.replace(/[a-zA-Z;,\n]/g,' ');
    let replace = replaceString.replace(/\s{1,}/g,',');
    let replaced = replace.split(',');
    replaced.forEach(element => {
      this.tagsArray.push(element);
    });
    localStorage.setItem('tags',JSON.stringify(this.tagsArray));
    this.tagsArray = JSON.parse(localStorage.getItem('tags'));
  }

  delete(index){
    this.tagsArray.splice(index,1);
    localStorage.setItem('tags',JSON.stringify(this.tagsArray));
    this.tagsArray = JSON.parse(localStorage.getItem('tags'));
  }

  openModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
    // console.log(this.tagsArray.join(','));
    this.updateAdCashForm.patchValue({
      tags: this.tagsArray.join(','),
    })
  }

  save(){
    this.tagsArray = [];
    let tags = this.updateAdCashForm.value.tags;
    let replaceString = tags.replace(/[a-zA-Z;,\n]/g,' ');
    let replace = replaceString.replace(/\s{1,}/g,',');
    let replaced = replace.split(',');
    replaced.forEach(element => {
      this.tagsArray.push(element);
    });
    localStorage.setItem('tags',JSON.stringify(this.tagsArray));
    this.tagsArray = JSON.parse(localStorage.getItem('tags'));
    this.modalRef.hide();
  }

}
